<%-- 
    Document   : index
    Created on : 13/10/2014, 12:34:59 PM
    Author     : Araujo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>




<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Servlet a JSP</title>
        
        <script type="text/javascript">
            function ValidaSoloNumeros() {
              if ((event.keyCode < 48) || (event.keyCode > 57)){
                 event.returnValue = false;
             }
             else{
                event.returnValue = true; 
                 
             }
            }
            
        </script>
        
    </head>
    <body>
        
        
        <h1>Introduzca sus datos para iniciar sesión</h1>
        
        <form action="login.jsp" method="POST">
             <div>Usuario</div>
            <input type="text" name="usuario" value="" />
            <div>Contraseña</div>
            <input type="password" name="password" value="" onkeypress="ValidaSoloNumeros()" />
            <div>
                <input type="submit" value="Conectar" /></div>
        </form>
    </body>
</html>
